/*
  This code shows how you can hide sensitive info
  from showing up in the CLI output
*/

locals {
  db_password = {
    admin = "myTF_RDS_mySQL_2021-Onica"
  }
}

output "db_password" {
  value = local.db_password
  # sensitive = true # this setting hides the db password
}