variable "ami" {
    type = string
    default = "ami-0c2b8ca1dad447f8a"
}

variable "type" {
    type = string 
    default = "t2.micro"
}

variable "tags" {
    type = map 
    default = {
        Name = "WebServer"
        Env = "Dev"
    }
}

variable "count-ec2" {
    type = number
    default = 5
}

variable "subnet-cidr-public" {
    type = string
    default = "10.0.2.0/24"
}