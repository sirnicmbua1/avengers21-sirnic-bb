resource "aws_instance" "dev-ec2" {
    ami = var.ami
    instance_type = var.type
    count = var.count-ec2
    tags = var.tags
}