
terraform {
  backend "s3" {
    bucket = "mbua-tf-modules"
    key = "mbua-dev-tfstate01/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "mbua-tf-modules-lockID"
  }
}
/*
when creating the Dynamp DB table, make sure to create partition key as "LockID".
*/ 

